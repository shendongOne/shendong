package org.shendong.dongshopping.wx.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class UtilOrder {
    public static synchronized String getMerchantCode() {
        StringBuilder billno = new StringBuilder();

        // 日期(格式:20080524)
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        billno.append(format.format(new Date()));

        // 生成5位格林治毫秒数
        String endStr = String.valueOf(new Date().getTime());
        billno.append((endStr.substring(endStr.length() - 4, endStr.length())));

        // 生成4位随机数
        String str4 = "0123456789";
        Random rand = new Random();
        int length = 2;
        for (int i = 0; i < length; i++) {
            int randNum = rand.nextInt(9);
            billno.append(str4.substring(randNum, randNum + 1));
        }
        return billno.toString();

    }
}
